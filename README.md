# Where is Mastodon?

Find where are mastodon instances hosting using the PTR record of the hostnames IP address.
The motivation is to guess how many of them are self-hosted and how many on corporate clouds.

![pie_chart](example_representation/summary.png)

If you liked this, please consider improving this by adding user weights to the hostnames,
thus answering the question "how many users on mastodon live on a corporate cloud", better
than "how many mastodon instances live on corporate cloud".
