#!/bin/bash

# Name: whereismastodon
# Author: fadelkon
# Date: May 2018
# Description: Find where are mastodon instances hosting using the PTR record of the hostnames IP address.

echo "Prepare environment"
DIR="data"
JSON="$DIR/instances.json"
DOMAINS="$DIR/domains.txt"
REPORT="$DIR/domains.csv"
SUMMARY="$DIR/host_count.txt"
rm "$DIR" "$JSON" "$DOMAINS" "$REPORT" "$SUMMARY" 2> /dev/null

mkdir -p "$DIR"

echo "Download data from instances.social"
COUNT=2000
curl "https://instances.social/api/1.0/instances/list?count=$COUNT&include_down=false&include_closed=false&min_version=2.1.2&min_active_users=1" \
-H "Authorization: Bearer vyIS19U6VB4YBzeSwxur4IKPnrhmX0kCAemSFDdrkPXC9ivCBYZWkyVNXqzZp22ev3zmVYOJhuJmIjHTOgalMF1LGBKI4EFQn2fI3cJ8e9v1ThCxTwFha7dbHoxDDNC6" | json_pp > "$JSON"

# Expected format
#         "checked_at" : "2018-05-15T17:29:30.273Z",
#         "name" : "mastodon.xyz",
#         "info" : {
#            "categories" : [],

echo "Get hostnames"
grep '\"name\"' $JSON | sed -r 's/[^:]+: "(.+)",?/\1/' > "$DOMAINS"

function add { echo -n "$1," >> "$REPORT"; }
function add_last { echo "$1," >> "$REPORT"; }

add "Hostname"
add "IP Address"
add "Reverse Hostname"
add_last "Short Reverse Hostname"

function investigate_host {
    HOST="$1"
    LINE="$HOST"

    echo "Investigate host $HOST" >&2

    IP="$(host "$HOST" | grep -v alias | head -n 1 | awk '{print $4}')"
    LINE="$LINE,$IP"

    H_R="$(host "$IP" | grep -v alias | head -n 1 | awk '{print $5}')"
    LINE="$LINE,$H_R"

    # Check hostname tld
    # Use `rev` (reverse line character-wise to get the last field separated by '.')
    # Expected format:
    # H_R="vpscloud.static.arena.ne.jp."
    TLD_R=$(echo "$H_R" | rev | cut -d . -f 2)
    TLD=$(echo "$TLD_R" | rev)
    if [[ "$TLD" == "jp" ]]; then
        # Japan tld has subdomains tld-like, so get 3 level subdomain (e.g. ne.jp are not distinctive)
        # May apply to others like uk and co, but they don't appear now.
        H_RS="$(echo "$H_R" | sed -r 's/([^\.,]+\.)*(([^\.,]+\.){3})/\2/')"
    else
        H_RS="$(echo "$H_R" | sed -r 's/([^\.,]+\.)*(([^\.,]+\.){2})/\2/')"
    fi
    LINE="$LINE,$H_RS"
    echo "$LINE"

    echo "Host $HOST is on $H_RS" >&2
}
echo "Process hostnames and resolve their IP adresses and IP reverse hostnames"
INDEX=0
TOTAL=$(wc -l "$DOMAINS" | cut -d ' ' -f 1)
while read -r HOST; do
    echo -n "[$INDEX/$TOTAL] "
    investigate_host "$HOST" >> "$REPORT" &
    INDEX=$((INDEX+1))
    sleep 0.2
done < "$DOMAINS"

wait

#Then count unique appearings
echo "Count how many instances has each reverse hostname"
grep -v "Hostname" "$REPORT" | cut -d ',' -f 4 | sort | uniq -c | sort -h | tr -s ' ' | tee "$SUMMARY"
